/* Philip Haubrich 1/11/2013
Code wheel. Just felt the need to blit something
Huh, pixel manipulation in SDL is annoying. And the libraries/forums are out of date.
Released to the public domain
*/

#define false 0
#define true 1
#define uint32 unsigned int
#define BUFFSIZE 1000

//The headers

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"


//Screen attributes
const int SCREEN_WIDTH = 600;
const int SCREEN_HEIGHT = 196;
const int SCREEN_BPP = 32;

const int FRAMES_PER_SECOND = 10;



//The surfaces
SDL_Surface *g_screen = NULL;     //Main surface displayed
SDL_Surface *g_message = NULL;    //Used for text
SDL_Surface *g_wheel = NULL;      //Column of letters for blitting

//Font
TTF_Font *g_font;
SDL_Color textColor = { 254, 255, 255 }; // R, G, B

//The event structure
SDL_Event g_event;

//String buffer for all the text. Fuck it, it's a demo
char g_buff[BUFFSIZE] = "xmtkojbmvkct";
int g_curserPos=12;

char g_bugBuff[100] = "Start typing. Use the Scrollwheel.";
int g_debug=0;
int g_targetOffset = 0;
int g_actualOffset = 0;

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination )
{
  SDL_Rect offset;
  SDL_Rect* clip = NULL;
  offset.x = x;
  offset.y = y;
  SDL_BlitSurface( source, clip, destination, &offset );
}

int load_files()
{
  g_font = TTF_OpenFont( "agencyfb-bold.ttf", 24);
  if( g_font == NULL )
  {
    return false;
  }

  g_wheel = IMG_Load("text.png");
  if(!g_wheel)
  {
    printf("IMG_Load: %s\n", IMG_GetError());
    return false;
  }
  return true;
}

int init()
{
  if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
  {
    return false;
  }
  g_screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );
  if( g_screen == NULL )
  {
    return false;
  }
  SDL_WM_SetCaption( "CodeWheel", NULL );

  //Initialize SDL_ttf
  if( TTF_Init() == -1 )
  {
    return false;
  }

  if( IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG)
  {
    printf("IMG_Init: Failed to init png support!\n");
    return false;
  }


  if( load_files() == -1 )
  {
    return false;
  }

  return true;
}



void clean_up()
{
  //Free the surfaces
  SDL_FreeSurface( g_screen );
  //SDL_FreeSurface( g_message );

  //Close the font that was used
  TTF_CloseFont( g_font );

  //Quit SDL_ttf
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();
}

//ALWAYS implimented EXACLY unlike how I expect it to work.
//Was that so fucking hard? I'm making a god damned mobius and my tools are stabbing me right in the back when I'm not looking. What the hell. Who fucking impliments the mod-op to return negative values. I will find you and flay you! Truncated division, you're on the list.
int philmod(int val, int mod)
{
  if(val%mod < 0)
  {
    //val = mod-(val%mod);
    return mod+(val%mod);
  }
  return val % mod;
}


//So the image I have is 14x468  A-Z, each being 14 x 18
void printLetter(char letter, int xPos)
{
  SDL_Rect offset;
  SDL_Rect clip;

  if(letter == ' ')
  { return; }


  clip.w = 14;
  clip.h = SCREEN_HEIGHT;
  clip.x = 0;
  clip.y =  philmod(g_actualOffset + (letter - 'a') * 18 ,468);

  //g_debug = clip.y;

  offset.x = xPos * 14;
  offset.y = 0;
  SDL_BlitSurface( g_wheel, &clip, g_screen, &offset );

  clip.y = (clip.y - 468);
  SDL_BlitSurface( g_wheel, &clip, g_screen, &offset );
}


void drawPixel(SDL_Surface* surface, Uint32 x, Uint32 y, Uint32 color)
{
  Uint32 bpp, offset;

  if(y >= SCREEN_HEIGHT) { return; }  //hmmmm, sorta ties it to g_screen... but meh
  if(x >= SCREEN_WIDTH) { return; }

  bpp = surface->format->BytesPerPixel;
  offset = surface->pitch*y + x*bpp;

  SDL_LockSurface(surface);
  memcpy(surface->pixels + offset, &color, bpp);
  SDL_UnlockSurface(surface);
}


void shade()
{
  int x,y;
  Uint32 bpp, color, darker;
  Uint8* pixeladdress;

  SDL_LockSurface(g_screen);
  bpp = g_screen->format->BytesPerPixel;
  for(y=0; y<SCREEN_HEIGHT/2; y++)
  {
    for(x=0; x<SCREEN_WIDTH; x++)
    {
      //darken percentage (h-i*2)/h
      pixeladdress = (Uint8*)g_screen->pixels;
      pixeladdress += (y * g_screen->pitch) + (x *bpp);
      color = *(Uint32*)pixeladdress;

      darker = 512.0*(float)y/(float)SCREEN_HEIGHT;
      darker = (darker<<0 | darker<<8 | darker<<16); //You know, while this works, I'm not proud of it.
      *((Uint32*)pixeladdress) = color & darker ;
    }
  }
  for(y=SCREEN_HEIGHT/2+1; y<SCREEN_HEIGHT; y++)
  {
    for(x=0; x<SCREEN_WIDTH; x++)
    {
      //darken percentage (h-i*2)/h
      pixeladdress = (Uint8*)g_screen->pixels;
      pixeladdress += (y * g_screen->pitch) + (x *bpp);
      color = *(Uint32*)pixeladdress;

      darker = 512.0*((float)SCREEN_HEIGHT-(float)y)/(float)SCREEN_HEIGHT;
      darker = (darker<<0 | darker<<8 | darker<<16);
      *((Uint32*)pixeladdress) = color & darker ;
    }
  }
  SDL_UnlockSurface(g_screen);

}


Uint32 animation( )
{
  static int oldTime = 0;
  int i;

  if( oldTime + FRAMES_PER_SECOND > SDL_GetTicks())
  { return 0; }
  oldTime = SDL_GetTicks();

  //Fill the screen with white / erase the last frame
  SDL_FillRect(g_screen,NULL, 0x000000);

  /*
  sprintf(g_bugBuff,"Debug: %d %d %d %s", g_targetOffset, g_actualOffset, g_debug, g_buff);
  g_message = TTF_RenderText_Solid( g_font, g_bugBuff, textColor );
  apply_surface( 200, SCREEN_HEIGHT*4.0/6.0, g_message, g_screen);
  */
  if(oldTime < 10000)
  {
    g_message = TTF_RenderText_Solid( g_font, g_bugBuff, textColor );
    apply_surface( 300, 20+oldTime/50, g_message, g_screen);
    SDL_FreeSurface(g_message);
  }
  
  for(i=0; g_buff[i] != 0 || i > BUFFSIZE; i++)
  {
    printLetter(g_buff[i], i);
  }

  //Outline of the central line of text
  for(i=0; i<SCREEN_WIDTH; i++)
  {
    drawPixel(g_screen,i,SCREEN_HEIGHT/2-10,0xffffffff);
    drawPixel(g_screen,i,SCREEN_HEIGHT/2+5,0xffffffff);
  }
  //Cursor
  for(i=SCREEN_HEIGHT/2-10; i<SCREEN_HEIGHT/2+5; i++)
  {
    drawPixel(g_screen,g_curserPos*14,i,0xffffffff);
  }

  //Add some shadow above and below the central line
  shade();


  //Smooth rotate
  if( g_actualOffset < g_targetOffset)
  {
    //Linear
    //g_actualOffset++;
    //Porportional
    g_actualOffset += ((g_targetOffset - g_actualOffset)*.1)+1;

  }
  if( g_actualOffset > g_targetOffset)
  {
    g_actualOffset -= ((g_actualOffset - g_targetOffset)*.1)+1;
  }

  if( SDL_Flip( g_screen ) == -1 )
  {
    printf("UpdateFAIL\n");
  }

  return 1;
}



void backspace()
{
  int i;
  if(g_curserPos == 0)
  { return; }
  for(i=g_curserPos; g_buff[i] != 0 || i < BUFFSIZE; i++)
  {
    g_buff[i-1] = g_buff[i];
  }
  g_curserPos--;
}

void insert(char c)
{
  int i;
  //I really jsut don't give a damn
  for(i=BUFFSIZE-1; i > g_curserPos; i--)
  {
    g_buff[i] = g_buff[i-1];
  }
  g_buff[i] = c;
  g_curserPos++;
}

void delete()
{
  int i;
  //I really jsut don't give a damn
  for(i=g_curserPos; g_buff[i] != 0 || i < BUFFSIZE-1; i++)
  {
    g_buff[i] = g_buff[i+1];
  }
}


//Called in Main
int handleKeyStroke()
{
  char c;
  int sym = g_event.key.keysym.sym;
  int mod = g_event.key.keysym.mod;

  printf("testing keys: %d %c\n",sym,sym);

  switch(sym)
  {
  case SDLK_ESCAPE:
    exit(0);
    ///TODO: add copy and paste too. Sadly, WIN-API here we come.
  case SDLK_BACKSPACE:
    backspace();
    break;
  case SDLK_DELETE: //Uh, but no arrow keys... so this is moot
    delete();
    break;
  default:
    if(sym >= 'a' && sym <= 'z')
    {
      //hmmm, kind of a last minute lame hack job. meh.
      sym = sym-'a';
      //sym -= 5;
      sym = philmod(sym-g_debug-5,26);
      sym = sym+'a';
      insert(sym);
    }
    if(sym == ' ')
    {
      insert(sym);
    }
  }
  return 0;
}

int handleEvent(int eventType)
{
  static int wheel;
  switch(eventType)
  {
  case SDL_QUIT:
    return true;
    break;

  case SDL_KEYDOWN:
    handleKeyStroke();
    break;

  case SDL_MOUSEBUTTONDOWN:
    if( g_event.button.button == 4) //mousewheel down
    {
      wheel--;
    }
    if( g_event.button.button == 5) //mousewheel up
    {
      wheel++;
    }
    g_debug = philmod(wheel,26);
    g_targetOffset = wheel * 18;

  default:
    ;
  }
  return false;
}



// MAIN!
int main( int argc, char* args[] )
{
  int quit = false;
  int deleting=0;
  int demoloop = 0;

  if( init() == false )
  {
    return 1;
  }

  while( quit == false )
  {
    while( SDL_PollEvent( &g_event ) )
    {
      quit = handleEvent(g_event.type);
    }
    animation();
    
    /*
    //Demo
    if( SDL_GetTicks() - demoloop > 500) 
    { 
      demoloop = SDL_GetTicks();
      if(g_curserPos < 5)
      {
        deleting = 0;
      }
      else if(g_curserPos > 40 || deleting)
      { 
        deleting = 1;
        backspace(); 
      }    
      switch(rand()%8)
      {
        case 0:
        case 7:
          insert(rand()%36+'a');
          break;
        case 1:
          g_targetOffset += 18;
          break;
        case 2:
          g_targetOffset -= 18;
          break;
        case 3:
          g_targetOffset += 18 * (rand()%10);
          break;
        case 4:
          g_targetOffset -= 18 * (rand()%10);  
          break;
        case 5:
          insert(' ');
          break;
        case 6:
          backspace();
          break;
        
      }
    }
    //End demo
    */
    
  }
  clean_up();

  return 0;
}
