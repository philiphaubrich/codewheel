Built in Mingw  http://www.mingw.org/

Dependencies:
SDL			http://www.libsdl.org/
SDL_image		http://www.libsdl.org/projects/SDL_image/
SDL_ttf		http://www.libsdl.org/projects/SDL_ttf/

Also note that there's no header file, so if you're going to actually use this for something you should declare all the function in a .h file rather than keeping everything sequentially dependent.

It's a little SDL demo, that's all you're getting out of me.
